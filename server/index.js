import express from "express";
import mongoose from "mongoose";
import cors from "cors";

import postsRoutes from './routes/posts.js'

const app = express()


app.use(express.json({limit: '30mb', extended: true}))
app.use(express.urlencoded({limit: '30mb', extended: true}))
app.use(cors())

app.use('/posts', postsRoutes)

const CONNECTION_URI = 'mongodb://localhost:27017/memories'
const PORT = process.env.PORT || 5000

mongoose.connect(CONNECTION_URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => app.listen(PORT, () => console.log(`Server running on port ${PORT}`)))
    .catch((e) => console.log(e.message))

mongoose.set('useFindAndModify', false)
